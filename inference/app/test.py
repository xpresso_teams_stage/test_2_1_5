import json
import numpy as np
with open('scratch.json') as json_file:
    data = json.load(json_file)
# cols=['img1', 'img2', 'img3', 'img4','img5']
from keras.preprocessing import image

list1=[]
list2=[]
for img in data['input']:
 list1.append(img)

for col in list1:
    list2.append(data['input'][col])

for img in list2:
    test_image=image.load_img(img,target_size=(64,64))
    test_image=image.img_to_array(test_image)
    test_image=np.expand_dims(test_image,axis=0)
