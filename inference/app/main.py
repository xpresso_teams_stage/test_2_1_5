"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""

import logging
import os
import numpy as np
import pickle
from keras.models import load_model
from keras.preprocessing import image

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="inference",
                   level=logging.INFO)


class Inference(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()
        self.model = None
        self.images = ['img1', 'img2', 'img3', 'img4','img5']
        """ Initialize any static data required during boot up """

    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """

        self.model = load_model(os.path.join(model_path, "cnn_model1.h5"))
        print("Model Loaded")


    def get_credentials(self):
        return {
            "uid": "asarkar",
            "pwd": "Asarkar@0309",
            "env": "qa"
        }

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """
        data_list = [input_request[col] for col in self.images]
        return data_list

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        results=[]
        for img in input_request:
            test_image = image.load_img(img, target_size=(64, 64))
            test_image = image.img_to_array(test_image)
            test_image = np.expand_dims(test_image, axis=0)
            result=self.model.predict(test_image)
            results.append(result)

        return results

    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        pred=[]
        for result in output_response:
            if result==1:
                pred.append('DOG')
            else:
                pred.append('CAT')

        dict = {}
        for i in range(len(self.images)):
            dict[self.images[i]] = pred[i]

        return dict



if __name__ == "__main__":
    pred = Inference()
    # === To run locally. Use load_model instead of load. ===
    # pred.load_model(model_path=""cnn_model.h5)
    pred.load()
    pred.run_api(port=5000)
